#!/bin/bash

chi_sq2(){
	#echo "calculating full entropy..."
	ent $1 -t > entro

	#echo -e "\nparsing chisquare..."
	csvtool format '%(4)\n' entro > entro2
	sed '2!d' entro2 > result
	rm entro*

	re=$( cat result )

	echo "$re" >> results 
}

#calculating chi2 of files in parameter folder
for i in $1/*; do chi_sq2 $i; done
rm result
#sorting from min to max
sort -n results > final
rm results

cat final

