# Manual for the PoC

## Understanding the content

The [chisq2.sh](https://gitlab.com/terceranexus6/ga-entropyanalysis/-/blob/main/PoC/chi2.sh) script will calculate the chi square value of the files inside a folder and sort them from the lower to the higher. The [ecrypt.sh](https://gitlab.com/terceranexus6/ga-entropyanalysis/-/blob/main/PoC/encrypt.sh) script will allow us to create new partially encrypted files (each 16 bytes as in Lockfile ransomware) for testing further examples. 

## Calculating the chi square of various files

First we will get the list of chi square values from example files. The values will be saved in a file called "final" that will be afterwards used. We calculated the mean of chi square values in regular example files previously, but just in case the tester want to calculate again is as easy as launching the chi square calculator script against a folder with regular files and calculate the mean of those values. Here's a helpful one liner for calculating the avarage, the max and the minimum values based on a file:

```

cat MYFILE | awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1<min) {min=$1}; total+=$1; count+=1} END {print total/count, max, min}'

```

## Calculating the minimal detection entropy value using GA

For convenience in the GA script we will save the mean value of the **regular** files in a file called "mean", although we can change that line in the python script and usethe value instead.

Finally the `ga_fit.py` script will calculate the minimum value for detection using as the fit function a new chi square in which the expected value is the regular mean and the min-max values to iterate through are the ones from the partially encrypted files.

## Full PoC 

```
$ for i in myfolderwithregularfiles/*;do ./encrypt.sh $i > ../partiallyencrypted/ 
$ ./chisq2.sh partiallyencrypted
$ echo $mean > mean
$ python3 ga_fit.py
```

The python script will throow both a graph with the evolution and the minimal value.



