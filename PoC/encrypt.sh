#!/bin/bash

count=0
myfile=$1

#lenght in bytes
lenght=$(wc -c $myfile | head -n1 | awk '{print $1;}')

touch $2

#iterating the parameter file
#then taking 16 bytes, encrypting them
#writing the next unencrypted bytes
#and increasing the encryption in order to jump 16 bytes more

while [ $count -lt $lenght ]; do
	od -j $count -N 16 -a $myfile | openssl enc -aes-256-cbc -md sha512 -a -pbkdf2 -iter 100000 \
		-salt -pass pass:tralari >> $2
	tail -c +$((count+16)) $myfile >> $2 
	count=$((count+32))
done

