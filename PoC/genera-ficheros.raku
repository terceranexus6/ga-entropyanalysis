#!/usr/bin/env raku

my $alfabeto-size = @*ARGS[0] // 23;
my $repeticiones-max = @*ARGS[1] // 16;
my $iteraciones = @*ARGS[2] // 128;

for <2 8 16 23> -> $alfabeto-size {
    for <16 32 64 128> -> $repeticiones-max {

	my @alfabeto = ('a'..'z')[^$alfabeto-size];
	my $fichero = '';
	for 0..$iteraciones {
	    $fichero ~= @alfabeto.pick x $repeticiones-max.rand ~ " ";
	}
	spurt( "{$alfabeto-size}_{$repeticiones-max}.txt", $fichero );
    }
}
	
