import numpy as np
from geneticalgorithm import geneticalgorithm as ga

with open('final') as f:
    for line in f:
        pass
    last_line = line
f.close()

first_line=open("final").readline().rstrip()
first_line=float(first_line)


#expe=float(first_line)
expe=open("media").readline().rstrip()
expe=float(expe)

last_line=float(last_line)

def f(X):
    res= np.sqrt(((X-expe)**2)-expe)
    return res

varbound=np.array([[first_line,last_line]]*1)

model=ga(function=f,dimension=1,variable_type='real',variable_boundaries=varbound)

model.run()
